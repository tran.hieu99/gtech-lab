// eslint-disable-next-line no-undef
module.exports = {
    extends: ['@commitlint/config-conventional'],
    rules: {
        'type-enum': [
            2,
            'always',
            ['build', 'chore', 'ci', 'docs', 'improvement', 'feature', 'fix', 'perf', 'refactor', 'revert', 'style', 'test', 'hotfix'],
        ],
    },
};