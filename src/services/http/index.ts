import axios, {
  AxiosInstance,
  AxiosPromise,
  AxiosRequestConfig,
  AxiosResponse,
} from "axios";

import { API_REQUEST_TIMEOUT } from "./contants";

enum LogType {
  REQUEST = "req",
  RESPONSE = "res",
  ERROR = "err",
}

const log = (...params: any) => {
  if (process.env.NODE_ENV === "development") {
    // eslint-disable-next-line no-console
    console.warn(...params);
  }
};

const requestLog = (
  method: string = "",
  url: string = "",
  data: any,
  type: LogType,
  baseURL: string
) => {
  const tag =
    type === LogType.REQUEST || type === LogType.RESPONSE
      ? method
      : LogType.ERROR;
  const colors = {
    [LogType.REQUEST]: "blue",
    [LogType.RESPONSE]: "green",
    [LogType.ERROR]: "red",
  };
  const icons = {
    [LogType.REQUEST]: ">>>",
    [LogType.RESPONSE]: "<<<",
    [LogType.ERROR]: "xxx",
  };

  log(
    `%c${icons[type]} [${tag.toUpperCase()}] | %c${url.replace(
      baseURL,
      ""
    )} \n`,
    `color: ${colors[type]}; font-weight: bold`,
    "color: violet; font-weight: bold",
    data
  );
};

const headers = {
  Authorization: "Basic",
  "Content-Type": "application/x-www-form-urlencoded",
};

abstract class HttpClient {
  protected readonly instance: AxiosInstance;
  protected refreshTokenRequest: AxiosPromise | null;
  protected requestQueue: AxiosRequestConfig[];
  protected requestQueueTime: AxiosRequestConfig[];

  public constructor(baseURL: string) {
    this.instance = axios.create({
      baseURL,
      headers,
      timeout: API_REQUEST_TIMEOUT,
    });
    this.refreshTokenRequest = null;
    this.requestQueue = [];
    this.requestQueueTime = [];
    this._initializeResponseInterceptor();
  }

  private _initializeResponseInterceptor = () => {
    this.instance.interceptors.request.use(
      this._handleRequest,
      this._handleRequestError
    );

    this.instance.interceptors.response.use(
      this._handleResponse,
      this._handleError
    );
  };

  private _handleRequest = () => {};

  private _handleRequestError = (error: any) => {
    log("request.error", error?.response?.data);
    return Promise.reject(error);
  };

  private _handleResponse = (response: AxiosResponse) => {
    const {
      config: { method, url, baseURL },
    } = response;
    requestLog(method, url, response, LogType.RESPONSE, baseURL || "");
    return response;
  };

  protected _handleError = () => {};

  public get = <T>(
    url: string,
    params = {},
    config: AxiosRequestConfig = {}
  ): AxiosPromise<T> => this.instance.get<T>(url, { params, ...config });

  public post = <T>(
    url: string,
    data: any = {},
    config: AxiosRequestConfig = {}
  ) => this.instance.post<T>(url, data, { ...config });

  public put = <T>(
    url: string,
    data: any = {},
    config: AxiosRequestConfig = {}
  ) => this.instance.put<T>(url, data, { ...config });

  public patch = <T>(
    url: string,
    data: any = {},
    config: AxiosRequestConfig = {}
  ) => this.instance.patch<T>(url, data, { ...config });

  public delete = <T>(url: string, config: AxiosRequestConfig = {}) =>
    this.instance.delete<T>(url, { ...config });
}

export default HttpClient;
