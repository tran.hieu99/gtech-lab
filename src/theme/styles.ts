import { theme } from "@chakra-ui/react";
import { mode } from "@chakra-ui/theme-tools";

export const globalStyles = {
  styles: {
    colors: {
      ...theme.colors,
    },
    global: (props: any) => ({
      body: {
        fontFamily: "body",
        color: mode("gray.800", "whiteAlpha.900")(props),
        bg: mode("white", "red.800")(props),
        lineHeight: "base",
      },
    }),
  },
};
