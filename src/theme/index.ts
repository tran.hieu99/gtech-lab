import { globalStyles } from "./styles";
import { extendTheme } from "@chakra-ui/react";
import breakpoints from "./foundations/breakpoints";
import config from "./foundations/config";
import colors from "./foundations/colors";
import typography from "./foundations/typography";
import spacing from "./foundations/spacing";
import sizes from "./foundations/sizes";
import radii from "./foundations/borderRadius";
const theme = extendTheme({
  ...breakpoints,
  ...config,
  ...colors,
  ...globalStyles,
  ...typography,
  ...spacing,
  ...sizes,
  ...radii,
});
export default theme;
