import { theme } from "@chakra-ui/react";
const colors = {
  colors: {
    ...theme.colors,
    primary: { ...theme.colors.blue },
  },
};
export default colors;
