import { Flex, Heading, Button, useColorMode } from "@chakra-ui/react";
import type { NextPage } from "next";

const Home: NextPage = () => {
  const { colorMode, toggleColorMode } = useColorMode();
  return (
    <Flex
      justifyContent="center"
      alignItems="center"
      flexDirection="column"
      height="100vh"
    >
      <Heading pb={5} color="textColor">
        Hello from G Tech App
      </Heading>
      <Button onClick={toggleColorMode}>Toggle color mode {colorMode}</Button>
    </Flex>
  );
};
export default Home;
